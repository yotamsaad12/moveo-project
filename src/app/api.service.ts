import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  userData:UserData=new UserData();
  usersInfo:UserData[]=[];
  //selectedUser:UserData=new UserData();
  constructor(private http:HttpClient) { }
  
  public selectedUser: UserData=new UserData();
  public get user() : UserData {
    return this.selectedUser;
  }
  public set user(v : UserData) {
    this.selectedUser = v;
  }
  


  getAllUsers():any{
    const headers = new HttpHeaders({
      'X-Requested-With': 'XMLHttpRequest'
    });
    var data=this.http.get<any[]>("http://randomuser.me/api/?results=5000",{'headers':headers});
    return data;
  }
}
export class UserData {
  picture:string="";
  name: string="";
  email: string="";
  gender: string="";
  age: number=0;

  constructor(){
  }
}
// const headers= new HttpHeaders()
//   .set('content-type', 'application/json')
//   .set('Access-Control-Allow-Origin', '*');
//   const cors = require('cors');
// const corsOptions ={
//     origin:'http://localhost:3000', 
//     credentials:true,            //access-control-allow-credentials:true
//     optionSuccessStatus:200
// }
// app.use(cors(corsOptions));