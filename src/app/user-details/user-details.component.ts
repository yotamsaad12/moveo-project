import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {ApiService} from '../api.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  private sub: any;
  public id: string="";
  public picture:string="";
  public name:string="";
  public email:string="";
  public gender:string="";
  public age:number=0;
  public userData:UserData= new UserData();
  @Input('userDataFromTable') userDataFromTable:UserData=new UserData();
 

  lat = 22.2736308;
  long = 70.7512555;
  constructor(private route: ActivatedRoute,private service:ApiService)
   {
     //this.userDataFromTable=this.service.selectedUser;

   }

   ngOnInit(){
      this.sub = this.route.params.subscribe(params => {
        debugger;
        this.id = params['id'];
        console.log(this.id);
        console.log(this.userDataFromTable.name);
        this.userDataFromTable=this.service.selectedUser;
        this.picture=this.userDataFromTable.picture;
        this.age=this.userDataFromTable.age;
        this.name=this.userDataFromTable.name;
        this.email=this.userDataFromTable.email;
        this.gender=this.userDataFromTable.gender;
        
    });

    

   }
  //  ngAfterViewInit() {
  //   this.sub = this.route.params.subscribe(params => {
  //     debugger;
  //     this.id = params['id'];
  //     console.log(this.id);
  //     console.log(this.userDataFromTable.name);
  //  });
   
   
  // }

  // ngOnInit() {
  // }
}
export class UserData {
  picture:string="";
  name: string="";
  email: string="";
  gender: string="";
  age: number=0;

  constructor(){
  }
}
