import { element } from 'protractor';
import { Router } from '@angular/router';
import {AfterViewInit, Component, ViewChild} from '@angular/core';
import { FormControl } from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {ApiService} from '../api.service';
import { Injectable } from '@angular/core';
import {UserDetailsComponent}  from '../user-details/user-details.component'// at top

@Injectable({
  providedIn: 'root' // just before your class
})
/**
 * @title Table with pagination
 */
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements AfterViewInit {
  displayedColumns: string[] = ['picture', 'name', 'email', 'gender','age'];
  dataSource = new MatTableDataSource<UserData>(ELEMENT_DATA);
  // toppings = new FormControl();
  // toppingList: string[] = ['name', 'age', 'email'];
  // name = new FormControl('');
  @ViewChild(MatPaginator) paginator: MatPaginator;
  clickedRows = new Set<UserData>();
  dataFromServer:any[]=[];
  userData:UserData;
  // @ViewChild(MatSort) sort: MatSort;

  constructor(private router: Router,private service:ApiService){

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    var data=this.service.getAllUsers().subscribe((data:any[])=>this.dataFromServer=data);
    //this.dataSource.sort = this.sort;
  }

  openDetails(row:any){
    debugger;
    this.userData=row;
    this.service.selectedUser=this.userData;
    this.router.navigate(['/user/'+row.name]);
    console.log(row.name);
  }
}

export interface UserData {
  picture:string,
  name: string;
  email: string;
  gender: string;
  age: number;
}

const ELEMENT_DATA: UserData[] = [
  {picture: '1', name: 'Hydrogen', email: '1', gender: 'H',age:1},
  {picture: '2', name: 'Helium', email: '4.0026', gender: 'He',age:1},
  {picture: '3', name: 'Lithium', email: '6.941', gender: 'Li',age:1},
  {picture: '4', name: 'Beryllium', email:' 9.0122', gender: 'Be',age:1},
  {picture: '5', name: 'Boron', email: '10.811', gender: 'B',age:1},
  {picture: '6', name: 'Carbon', email: '12.0107', gender: 'C',age:1},
  {picture: '7', name: 'Nitrogen', email: '14.0067', gender: 'N',age:1},
  {picture: '8', name: 'Oxygen', email: '15.9994', gender: 'O',age:1},
  {picture: '9', name: 'Fluorine', email: '18.9984', gender: 'F',age:1},
  {picture: '10', name: 'Neon', email: '20.1797', gender: 'Ne',age:1},
  {picture: '11', name: 'Sodium', email: '22.9897', gender: 'Na',age:1},
  {picture: '12', name: 'Magnesium', email: '24.305', gender: 'Mg',age:1},
  {picture: '13', name: 'Aluminum', email: '26.9815', gender: 'Al',age:1},
  {picture: '14', name: 'Silicon', email: '28.0855', gender: 'Si',age:1},
  {picture: '15', name: 'Phosphorus', email: '30.9738', gender: 'P',age:1},
  {picture: '16', name: 'Sulfur', email: '32.065', gender: 'S',age:1},
  {picture: '17', name: 'Chlorine', email: '35.453', gender: 'Cl',age:1},
  {picture: '18', name: 'Argon', email: '39.948', gender: 'Ar',age:1},
  {picture: '19', name: 'Potassium', email: '39.0983', gender: 'K',age:1},
  {picture: '20', name: 'Calcium', email: '40.078', gender: 'Ca',age:1},
];

